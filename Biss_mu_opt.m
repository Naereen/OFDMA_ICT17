function [mu_opt]       = Biss_mu_opt(channel_eff_used, Cap_const,mu_min, Band, m, P0, Ps, err)

%   This function computes use the bissection method in order to compute the
%   optimal service time as explained in our paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J. �Power Allocation for Minimizing Energy
%   Consumption of OFDMA Downlink with Cell DTx�, IEEE ICT, May 2017. 

%   Inputs:
%   channel_eff_used          : Channel gain allocated to each users 
%   Cap_const                 : Capacity constraint per user
%   mu_min                    : minimum service time
%   Band                      : bandwidth of each channel
%   m                         : slope of the load dependance
%   P0                        : Static power consumption of the BS
%   Ps                        : Power consumption of the BS during sleep mode
%   err                       : accepted error
%
%   Outputs:
%   mu_opt                    : Optimal service time

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/28/2016

mu_1                    = mu_min;
mu_2                    = 1;

Nu                      = size(channel_eff_used,1);
Nk                      = zeros(1,Nu);
for i =1:1:Nu
    Nk(1,i)                 = sum(channel_eff_used(i,:)>0);
end


Geom                    = zeros(1,Nu);
for i=1:1:Nu
    channel_eff_curr        = channel_eff_used(i,:);
    Geom(1,i)               = prod(channel_eff_curr(channel_eff_curr>0))^(1/Nk(1,i));
end

% First sum of the expression
Num11                   = 2.^(Cap_const./(Nk*Band*mu_1));
Num21                   = 2.^(Cap_const./(Nk*Band*mu_2));
Num12                   = (Cap_const*log(2))./(Band*mu_1)-Nk;
Num22                   = (Cap_const*log(2))./(Band*mu_2)-Nk;
Vect1                   = Num11.*Num12./Geom;
Vect2                   = Num21.*Num22./Geom;
sum1                    = sum(Vect1);
sum2                    = sum(Vect2);

% Second sum (inverse of the channel gains)
sum_inv                 = sum(1./channel_eff_used(channel_eff_used>0));

% Numerator
f1                      = sum1+sum_inv-(P0-Ps)/(m*Band);
f2                      = sum2+sum_inv-(P0-Ps)/(m*Band);

if f2>0,
    mu_1                = 1;
elseif f1<0,
    mu_2                = mu_min;
end

while abs(mu_2-mu_1)>err
    % Incrementation of the bissection method
    mu_int              = (mu_1+mu_2)/2;
    
    Num1_int            = 2.^(Cap_const./(Nk*Band*mu_int));
    Num2_int            = (Cap_const*log(2))./(Band*mu_int)-Nk;
    Vect_int            = Num1_int.*Num2_int./Geom;
    sum_int             = sum(Vect_int);
    
    f_int               = sum_int+sum_inv-(P0-Ps)/(m*Band);
    
    if f_int>0,
        mu_1            = mu_int;
    else
        mu_2            = mu_int;
    end
end

mu_opt              = mu_2;
end